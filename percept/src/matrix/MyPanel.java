/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matrix;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

/**
 *
 * @author oam
 */
public class MyPanel extends JPanel implements MouseListener, MouseMotionListener {

    public static final int IMW = 400;
    /*400 было*/
    public static final int IMH = 400;
    /*400-потому что 8x8 с шагом 50 :)
                                        как раз 400X400 в пикселях*/
    private int hstep = 50;
    /*10*/
    private int xsize, ysize;
    BufferedImage bim = new BufferedImage(IMW, IMH, BufferedImage.TYPE_INT_RGB);

    private int[][] matrix;
    private int sx, sy, nx, ny;
    //private boolean drflag = false;

    private int[] newMatrix;
    private int w = 8;
    private int h = 5;

    public MyPanel() {
        addMouseListener(this);
        addMouseMotionListener(this);
        init();
    }

    private void init() {
        Graphics2D g2 = bim.createGraphics();
        g2.setPaint(Color.WHITE);
        g2.fill(new Rectangle2D.Double(0, 0, IMW, IMH));
        xsize = IMW / hstep;   //M
        ysize = IMH / hstep;    //N
        matrix = new int[ysize][xsize];
        for (int i = 0; i < ysize; ++i) {
            for (int j = 0; j < xsize; ++j) {
                matrix[i][j] = 0;
            }
        }
        System.out.println("Построили сетку ");
    }

    /*  public void setHstep(int hstep){
        if(hstep<1 || hstep>100)hstep=10;
        this.hstep=hstep;
        init();
        repaint();
    }*/
    private void grid(Graphics2D g2) {
        g2.setPaint(Color.GRAY);
        for (int i = 0; i < IMW / hstep; ++i) {
            g2.draw(new Line2D.Double(i * hstep, 0, i * hstep, IMH));
        }
        for (int i = 0; i < IMH / hstep; ++i) {
            g2.draw(new Line2D.Double(0, i * hstep, IMW, i * hstep));
        }
    }

    private void square(int cx, int cy, Graphics2D g2) {
        g2.setPaint(Color.BLACK);
        g2.fill(new Rectangle2D.Double(cx * hstep, cy * hstep, hstep, hstep));
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        for (int i = 0; i < xsize; ++i) {
            for (int j = 0; j < ysize; ++j) {
                if (matrix[j][i] == 1) {
                    square(i, j, bim.createGraphics());
                }
            }
        }
        g2.drawImage(bim, null, 0, 0);
        grid(g2);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        /*  sx = e.getX();
        sy = e.getY();*/
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        /* if (drflag) {
            repaint();
        }
        drflag = false;*/
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {
        int cx = e.getX() / hstep;
        int cy = e.getY() / hstep;
        if (cx >= 0 && cy >= 0 && cx < xsize && cy < ysize) {
            matrix[cy][cx] = 1;
        }
        //System.out.println("Матрица " + matrix[cy][cx]);
        square(cx, cy, (Graphics2D) getGraphics());
//        drflag = true;
        // LetterRecognizer lr = new LetterRecognizer();
        //  newMatrix = new int[w*h];
//        newMatrix = lr.transformIntoArray(matrix);
    }

    public int[][] getMatrix() {
        return matrix;

    }

    /*public int[][] transform(int[][] matrix) {
    int[][] newMatr = matrix;
    /*  int nx = xsize / M;
    int ny = ysize / N;
    try {
    for (int i = 0; i < M; i++) {
    for (int j = 0; j < N; j++) {
    int sum = 0;
    for (int kx = (i * nx); kx < (i + 1) * nx; kx++) {
    for (int ky = (j * ny); ky < (j + 1) * ny; ky++) {
    sum += matrix[ky][kx];
    }
    }
    if (sum < (nx * ny) / 3) {
    newMatr[j][i] = 0;
    } else {
    newMatr[j][i] = 1;
    }
    }
    }
    } catch (Exception e) {
    e.printStackTrace();
    }
    return newMatr;
    }*/
    public void setMatrix(int[][] matrix) {
        this.matrix = matrix;
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }

}
