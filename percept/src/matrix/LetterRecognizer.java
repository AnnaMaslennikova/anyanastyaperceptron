/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matrix;

//import java.util.Arrays;

import static java.lang.Math.exp;

/**
 *
 * @author Anna2
 */
public class LetterRecognizer {
    
    private static final double speed = 0.005; //0.01
    private static final double apsilon = 0.01;
    private int [] line;
    
    public void makeLine(int matrix[][], int N, int M){
        line = transformIntoArray(matrix, N, M);
    }
    

    public int[] transformIntoArray(int matrix[][], int N, int M) {
     /*   int N = 8; 
        int M = 8; */
        int vector [] = new int[M*N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                vector[i * M + j] = matrix[i][j];
            }
        }
        return vector;
    } 
    
    public double[] receiveWeights(int len){
        double weights[] = new double[len];
        for(int i=0; i<len; i++){
            weights[i] = Math.random();
        }
        return weights;
    }
    
    public double[][] receiveWeights(int N, int M){
        double weights[][] = new double[N][M];
        for(int i=0; i<N; i++){
            for(int j=0; j<M; j++){
                weights[i][j] = Math.random();
            }
           
        }
        return weights;
    }
    
  //  public double returnF (int )
    
    public void receiveY(int N, int M, double w[], int x[]){
        double y[] = new double[M];
        double sumXW = 0.0;
        for(int i=0; i<N;i++){
            sumXW = x[i]*w[i];
        }
        double yy = 1/(1 + exp(-sumXW));
        
        
    }
    
    
}
