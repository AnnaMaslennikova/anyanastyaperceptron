package percept;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Line2D;
import javax.swing.JPanel;

/**
 *
 * @author Anastasia
 */
public class MyPanel extends JPanel implements MouseListener, MouseMotionListener {

    boolean flag = false;
    int cx, cy, ox, oy;
    // Размер игрового поля
    public static final int FIELD_SIZE = 360;
    // Начальное значение ячеек поля
    public final String NOT_SIGN = "*";
    static int linesCount = 6;
    int cellSize;
    // Наше поле
    public String[][] cell;

    public MyPanel() {
        addMouseListener(this);
        addMouseMotionListener(this);
        cellSize = FIELD_SIZE / linesCount;
        cell = new String[linesCount][linesCount];
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        // Рисуем линии, которые представляют собой сетку
        for (int i = 0; i <= linesCount; i++) {
            g.drawLine(0, i * cellSize, FIELD_SIZE, i * cellSize);
            g.drawLine(i * cellSize, 0, i * cellSize, FIELD_SIZE);
        }
        if (cx < FIELD_SIZE && cx > 0) {
            System.out.println("В области2");
        } else {
            System.out.println("НЕ области2");
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        flag = true;
        ox = e.getX();
        System.out.println("Значение х->>" + ox);
        oy = e.getY();
        System.out.println("Значение y->>" + oy);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        flag = false;
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {
        //для рисования буквы
        Graphics2D g2 = (Graphics2D) this.getGraphics();
        g2.setPaint(Color.BLUE);
        cx = e.getX();
        System.out.println("Значение х2->>" + cx);
        cy = e.getY();
        System.out.println("Значение y2->>" + cx);
        if (cx < FIELD_SIZE && cx > 0) {
            System.out.println("В области");
        } else {
            System.out.println("НЕ области2");
        }
        g2.draw(new Line2D.Double(ox, oy, cx, cy));
        ox = cx;
        oy = cy;
    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

}
